### Dev compile
$ npx mix
### Dev compile and watch
$ npx mix watch
### Production compile
$ npx mix --production
#### więcej o NPX
https://laravel-mix.com/docs/6.0/cli
